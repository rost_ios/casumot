//
//  UIBuilder.swift
//  CasumoTest
//
//  Created by Rost on 7/31/19.
//  Copyright © 2019 Rost Gress. All rights reserved.
//

import Foundation
import UIKit


class UIBuilder {

    
    /// ---> Make custom refresh control <--- ///
    static func makeRefreshControl(_ target: Any, selector: String) -> UIRefreshControl {
        let refreshControl = UIRefreshControl()
        refreshControl.tintColor = refreshControlColor
        let controlFont = UIFont.systemFont(ofSize: 16)
        let controlAttributes = [NSAttributedString.Key.font: controlFont]
        refreshControl.attributedTitle = NSAttributedString(string: "Fetching Events ...", attributes: controlAttributes)
        refreshControl.addTarget(target,
                                 action: NSSelectorFromString(selector),
                                 for: .valueChanged)
        
        return refreshControl
    }
    
    
    /// ---> Make custom button for navigation bar <--- ///
    static func makeNavBarButton(_ image: String, target: Any, selector: String) -> UIBarButtonItem {
        return UIBarButtonItem(image: UIImage(named: image),
                                       style: .plain ,
                                       target: target,
                                       action: NSSelectorFromString(selector))
    }
    
    
    /// ---> Make back button item for navigation bar <--- ///
    static func makeBackItem(_ nc: UINavigationController) {
        let backItem = UIBarButtonItem()
        backItem.title = "Back"
        nc.navigationBar.topItem?.backBarButtonItem = backItem
    }
    
    
    /// ---> Add empty footer view to table  <--- ///
    static func addEmptyFooter(_ table: UITableView) {
        let footerView = UIView()
        table.tableFooterView = footerView
    }
}
