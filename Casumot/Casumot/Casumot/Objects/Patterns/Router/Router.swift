//
//  Router.swift
//  CasumoTest
//
//  Created by Rost on 7/31/19.
//  Copyright © 2019 Rost Gress. All rights reserved.
//

import Foundation
import UIKit


class Router {
    
    
    /// ---> Show details view controller <--- ///
    static func showDetails(_ nc: UINavigationController) {
        pushNext("Details", in: nc)
    }
    
    
    /// ---> Show unit view controller <--- ///
    static func showUnit(_ nc: UINavigationController) {
        pushNext("Unit", in: nc)
    }
    
    
    /// ---> Show web view controller <--- ///
    static func showWeb(_ nc: UINavigationController) {
        pushNext("Web", in: nc)
    }
    
    /// ---> Show web view controller <--- ///
    static func showFollowers(_ nc: UINavigationController) {
        pushNext("Followers", in: nc)
    }
    
    
    /// ---> Push next view controller <--- ///
    static func pushNext(_ name: String, in nc: UINavigationController) {
        let nextStoryboard = UIStoryboard(name: name, bundle: nil)
        let nextViewController = nextStoryboard.instantiateViewController(withIdentifier: name + "ViewController")
        nc.pushViewController(nextViewController, animated: true)
    }  
}
