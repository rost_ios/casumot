//
//  SortView.swift
//  CasumoTest
//
//  Created by Rost on 8/1/19.
//  Copyright © 2019 Rost Gress. All rights reserved.
//

import Foundation
import UIKit


class SortView: UIView {
    @IBOutlet weak var sortTable: UITableView!
    @IBOutlet weak var okButton: UIButton!
    @IBOutlet weak var cleanButton: UIButton!
    var viewModel: SortViewModel!
    let sortTitles = ["Date Asc", "Date Desc", "Type Asc", "Type Desc", "Actor Asc", "Actor Desc", "Repository Asc", "Repository Desc"]
    
    /// ---> View life cycle <--- ///
    override func awakeFromNib() {
        super.awakeFromNib()
        
        viewModel = SortViewModel()
        
        viewModel.setupUI(self)
    }
}

