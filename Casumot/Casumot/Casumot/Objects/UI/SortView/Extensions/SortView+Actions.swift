//
//  SortView+Actions.swift
//  CasumoTest
//
//  Created by Rost on 8/1/19.
//  Copyright © 2019 Rost Gress. All rights reserved.
//

import Foundation
import UIKit


extension SortView {
    
    
    /// ---> Action for ok button <--- ///
    @IBAction func okButtonTapped(_ sender: UIButton) {
        viewModel.handleOkButton(self)
    }
    
    
    /// ---> Action for clean filter button  <--- ///
    @IBAction func cleanButtonTapped(_ sender: UIButton) {
        viewModel.handleCleanButton(self)
    }
}
