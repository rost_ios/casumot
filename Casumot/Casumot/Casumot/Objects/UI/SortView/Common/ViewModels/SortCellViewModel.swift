//
//  SortCellViewModel.swift
//  Casumot
//
//  Created by Rost on 28.01.2020.
//  Copyright © 2020 Rost Gress. All rights reserved.
//

import Foundation
import UIKit


class SortCellViewModel {
    
    /// ---> Setter for values in UI <--- ///
    func setCellValues(_ cell: SortCell, title: String) {
        cell.sortTitle.text = title
    }
}
