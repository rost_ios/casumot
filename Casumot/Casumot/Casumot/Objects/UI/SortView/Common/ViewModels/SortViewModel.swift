//
//  SortViewModel.swift
//  Casumot
//
//  Created by Rost on 28.01.2020.
//  Copyright © 2020 Rost Gress. All rights reserved.
//

import Foundation
import UIKit


class SortViewModel {
        
    /// ---> Add customizations for UI <--- ///
    func setupUI(_ view: SortView) {
        let footerView = UIView()
        view.sortTable.tableFooterView = footerView
        
        view.roundCorners(10, border: 2, color: .lightGray)
        view.okButton.roundCorners(10, border: 2, color: .white)
        view.cleanButton.roundCorners(10, border: 2, color: .white)
    }
    
    
    /// ---> Show or hide sort view <--- ///
    func showHideView(_ view: SortView) {
        if view.isHidden == true {
            view.alpha      = 0.0
            view.isHidden   = false
            
            if let index = DataContainer.shared.selectedSortIndex {
                let indexPath = IndexPath(row: index, section: 0)
                view.sortTable.selectRow(at: indexPath,
                                         animated: true,
                                         scrollPosition: .none)
            }
            
            UIView.animate(withDuration: 0.6, delay: 0, options: .curveEaseInOut, animations: {
                view.alpha = 1.0
            }) { (isCompleted) in
            }
        } else {
            UIView.animate(withDuration: 0.6, delay: 0, options: .curveEaseInOut, animations: {
                view.alpha = 0.0
            }) { (isCompleted) in
                view.isHidden = true
            }
        }
    }
    
    
    /// ---> Setter for selected state as checkmark <--- ///
    func setSelectedCheckmark(_ view: SortView, row: Int) {
        if let cell = getSortCell(view, row: row) {
            cell.accessoryType = .checkmark
        }
    }


    /// ---> Setter for deselected state <--- ///
    func setDeselect(_ view: SortView) {
        for index in 0 ..< view.sortTitles.count {
            if let cell = getSortCell(view, row: index) {
                cell.accessoryType = .none
            }
        }
    }
    
    
    /// ---> Getter for sort cell by selected index <--- ///
    func getSortCell(_ view: SortView, row: Int) -> SortCell? {
        let indexPath = IndexPath(row: row, section: 0)
        if let cell = view.sortTable.cellForRow(at: indexPath) as? SortCell {
            return cell
        }

        return nil
    }
    
    
    /// ---> Function for handle tap by Ok button <--- ///
    func handleOkButton(_ view: SortView) {
        showHideView(view)
        
        NotificationCenter.default.post(name: filterObserverName, object: nil)
    }
    
    
    /// ---> Function for handle tap by Clean button <--- ///
    func handleCleanButton(_ view: SortView) {
        if let selected = DataContainer.shared.selectedSortIndex {
            let indexPath = IndexPath(row: selected, section: 0)
            
            view.sortTable.deselectRow(at: indexPath, animated: true)
            
            setDeselect(view)
        }
        
        DataContainer.shared.selectedSortIndex = nil
        
        NotificationCenter.default.post(name: filterObserverName, object: nil)
    }
    
    
    /// ---> Function for make custom cells <--- ///
    func makeCell(_ table: UITableView, array: [String], index: IndexPath) -> UITableViewCell {
        if let cell = table.dequeueReusableCell(withIdentifier: "SortCell", for: index) as? SortCell {
            let typeTitle = array[index.row]
            
            if let model = cell.viewModel {
                model.setCellValues(cell, title: typeTitle)
            }
            
            return cell
        }
    
        return SortCell()
    }
    
    
    /// ---> Make height  of row <--- ///
    func makeRowHeight() -> CGFloat {
        return 48.0
    }
    
    
    /// ---> Make counts of events <--- ///
    func makeRowsCount(_ view: SortView) -> Int {
        return view.sortTitles.count
    }
    
    
    /// ---> Function for present details view <--- ///
    func handleSelectRow(_ view: SortView, index: IndexPath) {
        setDeselect(view)
        
        setSelectedCheckmark(view, row: index.row)

        DataContainer.shared.selectedSortIndex = index.row
    }
}
