//
//  SortCell.swift
//  CasumoTest
//
//  Created by Rost on 8/1/19.
//  Copyright © 2019 Rost Gress. All rights reserved.
//

import Foundation
import UIKit


class SortCell: UITableViewCell {
    @IBOutlet weak var sortTitle: UILabel!
    var viewModel: SortCellViewModel!
    
    /// ---> View life cycle <--- ///
    override func awakeFromNib() {
        super.awakeFromNib()
        
        viewModel = SortCellViewModel()
    }
    
}
