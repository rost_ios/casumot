//
//  DataContainer.swift
//  CasumoTest
//
//  Created by Rost on 7/30/19.
//  Copyright © 2019 Rost Gress. All rights reserved.
//

import Foundation


class DataContainer {
    static let shared = DataContainer()
    
    var selectedEvent: EventStruct? // Event which selected in events table
    
    var selectedSortIndex: Int?     // Index which selected in table on SortView
    
    var actorInfoUnit: ActorInfoStruct?     // Downloaded info about actor
    var orgInfoUnit: OrgInfoStruct?         // Downloaded info about organization
    
    var selectedUnit: UnitTypes?    // Selected unit for pass to UnitViewController
    
    var followersArray: [FollowerUserStruct]?   // List of followers for show Followers page
    
    var webTitle: String?           // Title for web page
    var webUrl: String?             // Selected url for pass to WebViewController
    
    private init() { }
}
