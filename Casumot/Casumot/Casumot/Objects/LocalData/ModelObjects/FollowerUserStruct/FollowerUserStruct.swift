//
//  FollowerUserStruct.swift
//  CasumoTest
//
//  Created by Rost on 8/2/19.
//  Copyright © 2019 Rost Gress. All rights reserved.
//

import Foundation


struct FollowerUserStruct {
    var avaUrl: String      = ""
    var userName: String    = ""
    var htmlUrl: String     = ""
}
