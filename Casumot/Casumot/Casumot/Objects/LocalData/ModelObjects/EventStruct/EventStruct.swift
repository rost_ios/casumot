//
//  EventStruct.swift
//  CasumoTest
//
//  Created by Rost on 7/30/19.
//  Copyright © 2019 Rost Gress. All rights reserved.
//

import Foundation


struct EventStruct {
    var eventId: String                 = ""
    var eventType: String               = ""
    var eventActor: ActorStruct?        = nil
    var eventRepo: RepoStruct?          = nil
    var eventOrg: OrgStruct?            = nil
    var isPublic: Bool                  = true
    var createdDate: String             = ""
}
