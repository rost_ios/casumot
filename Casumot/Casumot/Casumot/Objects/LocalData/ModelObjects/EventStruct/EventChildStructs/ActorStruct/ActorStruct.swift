//
//  ActorStruct.swift
//  CasumoTest
//
//  Created by Rost on 7/30/19.
//  Copyright © 2019 Rost Gress. All rights reserved.
//

import Foundation


struct ActorStruct {
    var actorId: String         = ""
    var displayLogin: String    = ""
    var actorLogin: String      = ""
    var avaUrl: String          = ""
    var actorUrl: String        = ""
}
