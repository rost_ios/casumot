//
//  OrgStruct.swift
//  CasumoTest
//
//  Created by Rost on 7/30/19.
//  Copyright © 2019 Rost Gress. All rights reserved.
//

import Foundation


struct OrgStruct {
    var orgId: String       = ""
    var orgLogin: String    = ""
    var avaUrl: String      = ""
    var orgUrl: String      = ""
}
