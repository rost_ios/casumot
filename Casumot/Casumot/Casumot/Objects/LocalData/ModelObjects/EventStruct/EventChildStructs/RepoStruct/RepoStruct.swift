//
//  RepoStruct.swift
//  CasumoTest
//
//  Created by Rost on 7/30/19.
//  Copyright © 2019 Rost Gress. All rights reserved.
//

import Foundation


struct RepoStruct {
    var repoId: String       = ""
    var repoName: String     = ""
    var repoUrl: String      = ""
}
