//
//  OrgInfoStruct.swift
//  CasumoTest
//
//  Created by Rost on 7/30/19.
//  Copyright © 2019 Rost Gress. All rights reserved.
//

import Foundation


struct OrgInfoStruct {
    var htmlUrl: String     = ""
    var homepageUrl: String    = ""
}

