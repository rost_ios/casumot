//
//  ActorInfoStruct.swift
//  CasumoTest
//
//  Created by Rost on 8/2/19.
//  Copyright © 2019 Rost Gress. All rights reserved.
//

import Foundation


struct ActorInfoStruct {
    var blogUrl: String        = ""
    var htmlUrl: String        = ""
    var followersUrl: String   = ""
}
