//
//  ActorInfoMapper.swift
//  CasumoTest
//
//  Created by Rost on 8/2/19.
//  Copyright © 2019 Rost Gress. All rights reserved.
//

import Foundation


class ActorInfoMapper {
    
    
    /// ---> Mapping info of actor json  <--- ///
    static func make(_ json: [String: Any]) -> ActorInfoStruct {
        var actorObject = ActorInfoStruct()
        
        if let value = json["html_url"] as? String {
            actorObject.htmlUrl = value
        }

        if let value = json["blog"] as? String {
            actorObject.blogUrl = value
        }
        
        if let value = json["followers_url"] as? String {
            actorObject.followersUrl = value
        }
        
        return actorObject
    }
}
