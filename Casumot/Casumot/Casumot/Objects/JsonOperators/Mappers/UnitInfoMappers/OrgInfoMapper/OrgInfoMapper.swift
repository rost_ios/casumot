//
//  OrgInfoMapper.swift
//  CasumoTest
//
//  Created by Rost on 8/2/19.
//  Copyright © 2019 Rost Gress. All rights reserved.
//

import Foundation


class OrgInfoMapper {
    
    /// ---> Mapping info of org json  <--- ///
    static func make(_ json: [String: Any]) -> OrgInfoStruct {
        var orgObject = OrgInfoStruct()
        
        if let value = json["html_url"] as? String {
            orgObject.htmlUrl = value
        }
        
        if let value = json["homepage"] as? String {
            orgObject.homepageUrl = value
        }
        
        return orgObject
    }
}
