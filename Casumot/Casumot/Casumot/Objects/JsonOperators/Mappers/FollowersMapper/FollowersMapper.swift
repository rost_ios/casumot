//
//  FollowersMapper.swift
//  CasumoTest
//
//  Created by Rost on 8/2/19.
//  Copyright © 2019 Rost Gress. All rights reserved.
//

import Foundation


class FollowersMapper {
    
    
    /// ---> Mapping followers json  <--- ///
    static func make(_ json: [String: Any]) -> FollowerUserStruct {
        var followerObject = FollowerUserStruct()

        if let value = json["avatar_url"] as? String {
            followerObject.avaUrl = value
        }
        
        if let value = json["login"] as? String {
            followerObject.userName = value
        }
        
        if let value = json["html_url"] as? String {
            followerObject.htmlUrl = value
        }

        return followerObject
    }
}
