//
//  ActorMapper.swift
//  CasumoTest
//
//  Created by Rost on 7/30/19.
//  Copyright © 2019 Rost Gress. All rights reserved.
//

import Foundation


class ActorMapper {
    
    
    /// ---> Mapping actor json  <--- ///
    static func make(_ json: [String: Any]) -> ActorStruct {
        var actorObject = ActorStruct()
        
        if let value = json["id"] as? String {
            actorObject.actorId = value
        }
        
        if let value = json["login"] as? String {
            actorObject.actorLogin = value
        }
        
        if let value = json["url"] as? String {
            actorObject.actorUrl = value
        }
        
        if let value = json["display_login"] as? String {
            actorObject.displayLogin = value
        }
        
        if let value = json["avatar_url"] as? String {
            actorObject.avaUrl = value
        }
        
        return actorObject
    }
}
