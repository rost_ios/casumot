//
//  RepoMapper.swift
//  CasumoTest
//
//  Created by Rost on 7/30/19.
//  Copyright © 2019 Rost Gress. All rights reserved.
//

import Foundation


class RepoMapper {
    
    
    /// ---> Mapping repo json  <--- ///
    static func make(_ json: [String: Any]) -> RepoStruct {
        var repoObject = RepoStruct()
        
        if let value = json["id"] as? String {
            repoObject.repoId = value
        }
        
        if let value = json["name"] as? String {
            repoObject.repoName = value
        }
        
        if let value = json["url"] as? String {
            repoObject.repoUrl = value
        }
        
        return repoObject
    }
}
