//
//  OrgMapper.swift
//  CasumoTest
//
//  Created by Rost on 7/30/19.
//  Copyright © 2019 Rost Gress. All rights reserved.
//

import Foundation


class OrgMapper {
    
    
    /// ---> Mapping org json  <--- ///
    static func make(_ json: [String: Any]) -> OrgStruct {
        var orgObject = OrgStruct()
        
        if let value = json["id"] as? String {
            orgObject.orgId = value
        }
        
        if let value = json["login"] as? String {
            orgObject.orgLogin = value
        }
        
        if let value = json["avatar_url"] as? String {
            orgObject.avaUrl = value
        }
        
        if let value = json["url"] as? String {
            orgObject.orgUrl = value
        }

        return orgObject
    }
}
