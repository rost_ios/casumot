//
//  EventsMapper.swift
//  CasumoTest
//
//  Created by Rost on 7/30/19.
//  Copyright © 2019 Rost Gress. All rights reserved.
//

import Foundation


class EventsMapper {
    
    
    /// ---> Mapping events json  <--- ///
    static func make(_ json: [String: Any]) -> EventStruct {
        var eventObject = EventStruct()
        
        if let value = json["id"] as? String {
            eventObject.eventId = value
        }
        
        if let value = json["type"] as? String {
            eventObject.eventType = value.replacingOccurrences(of: "Event", with: " Event")
        }
        
        if let value = json["actor"] as? [String: Any] {
            eventObject.eventActor  = ActorMapper.make(value)
        }
        
        if let value = json["repo"] as? [String: Any] {
            eventObject.eventRepo   = RepoMapper.make(value)
        }
        
        if let value = json["repo"] as? [String: Any] {
            eventObject.eventOrg    = OrgMapper.make(value)
        }
        
        if let value = json["public"] as? Bool {
            eventObject.isPublic    = value
        }
        
        if let value = json["created_at"] as? String {
            if value != "null" {
                let resultDate = Date.convert(value,
                                              income: "yyyy-MM-dd'T'HH:mm:ss'Z'",
                                              result: "dd MMMM YYYY, HH:mm")
                
                eventObject.createdDate = resultDate
            }
        }

        return eventObject
    }
}
