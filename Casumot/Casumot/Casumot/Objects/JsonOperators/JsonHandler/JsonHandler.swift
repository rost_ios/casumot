//
//  JsonHandler.swift
//  CasumoTest
//
//  Created by Rost on 7/30/19.
//  Copyright © 2019 Rost Gress. All rights reserved.
//

import Foundation


class JsonHandler {
    
    
    /// ---> Handle event json function  <--- ///
    static func handleEvents(_ json: [[String: Any]]) -> [EventStruct] {
        var handledResult: [EventStruct] = []

        for values in json {
            let eventObject = EventsMapper.make(values)
            
            handledResult.append(eventObject)
        }
        
        return handledResult
    }
    
    
    /// ---> Handle followers json function  <--- ///
    static func handleFollowers(_ json: [[String: Any]]) -> [FollowerUserStruct] {
        var handledResult: [FollowerUserStruct] = []
        
        for values in json {
            let followerObject = FollowersMapper.make(values)
            
            handledResult.append(followerObject)
        }
        
        return handledResult
    }
    
    
    /// ---> Handle actor json function  <--- ///
    static func handleActor(_ json: [String: Any]) -> ActorInfoStruct {
        let handledResult: ActorInfoStruct = ActorInfoMapper.make(json)

        return handledResult
    }
    
    
    /// ---> Handle org json function  <--- ///
    static func handleOrg(_ json: [String: Any]) -> OrgInfoStruct {
        let handledResult: OrgInfoStruct = OrgInfoMapper.make(json)
        
        return handledResult
    }  
}
