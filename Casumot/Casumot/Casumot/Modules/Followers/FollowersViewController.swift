//
//  FollowersViewController.swift
//  CasumoTest
//
//  Created by Rost on 8/2/19.
//  Copyright © 2019 Rost Gress. All rights reserved.
//

import Foundation
import UIKit


class FollowersViewController: UIViewController {
    @IBOutlet weak var followersTable: UITableView!
    var dataArray: [FollowerUserStruct] = []
    var viewModel: FollowersViewModel!
    
    
    /// ---> View life cycle <--- ///
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        viewModel = FollowersViewModel()
        
        viewModel.changeUI(self)
    }
}
