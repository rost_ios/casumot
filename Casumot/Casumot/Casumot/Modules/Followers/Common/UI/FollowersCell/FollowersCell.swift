//
//  FollowersCell.swift
//  CasumoTest
//
//  Created by Rost on 8/2/19.
//  Copyright © 2019 Rost Gress. All rights reserved.
//

import Foundation
import UIKit


class FollowersCell: UITableViewCell {
    @IBOutlet weak var avaImage: AsyncImageView!
    @IBOutlet weak var userTitle: UILabel!
    var viewModel: FollowersCellViewModel!
    
    /// ---> View life cycle <--- ///
    override func awakeFromNib() {
        super.awakeFromNib()
        
        viewModel = FollowersCellViewModel()
    }
}
