//
//  FollowersViewModel.swift
//  Casumot
//
//  Created by Rost on 28.01.2020.
//  Copyright © 2020 Rost Gress. All rights reserved.
//

import Foundation
import UIKit


class FollowersViewModel {
    
    /// ---> Add customizations for UI <--- ///
    func changeUI(_ view: FollowersViewController) {
        if let nc = view.navigationController {
            UIBuilder.makeBackItem(nc)
        }
        
        view.title = "Followers"
        
        UIBuilder.addEmptyFooter(view.followersTable)
        
        if let followers = DataContainer.shared.followersArray {
            view.dataArray = followers
            
            view.followersTable.reloadData()
        }
    }
    
    
    /// ---> Function for make custom cells <--- ///
    func makeCell(_ table: UITableView, array: [FollowerUserStruct], index: IndexPath) -> UITableViewCell {
        if let cell = table.dequeueReusableCell(withIdentifier: "FollowersCell", for: index) as? FollowersCell {
            let object = array[index.row]
            
            if let model = cell.viewModel {
                model.setCellValues(cell, object: object)
            }
            
            return cell
        }
        
        return FollowersCell()
    }
    
    
    /// ---> Make height  of row <--- ///
    func makeRowHeight() -> CGFloat {
        return 60.0
    }
    
    
    /// ---> Make counts of events <--- ///
    func makeRowsCount(_ view: FollowersViewController) -> Int {
        return view.dataArray.count
    }
    
    
    /// ---> Function for present details view <--- ///
    func presentDetails(_ view: FollowersViewController, index: IndexPath) {
        let selectedObject = view.dataArray[index.row]
        
        DataContainer.shared.webTitle = selectedObject.userName.shiftFirstLetter()
        DataContainer.shared.webUrl   = selectedObject.htmlUrl
        
        if let nc = view.navigationController {
            Router.showWeb(nc)
        }
    }
}
