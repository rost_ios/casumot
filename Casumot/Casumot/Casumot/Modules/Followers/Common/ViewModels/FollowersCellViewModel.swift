//
//  FollowersCellViewModel.swift
//  Casumot
//
//  Created by Rost on 28.01.2020.
//  Copyright © 2020 Rost Gress. All rights reserved.
//

import Foundation
import UIKit


class FollowersCellViewModel {
    
    /// ---> Setter for values in UI <--- ///
    func setCellValues(_ cell: FollowersCell, object: FollowerUserStruct) {
        let holderImage = UIImage(named: "holder_image")
        
        if !object.avaUrl.isEmpty {
            cell.avaImage.loadAsync(object.avaUrl, placeholder: holderImage)
        } else {
            cell.avaImage.image = holderImage
        }
        
        cell.avaImage.roundCorners(cell.avaImage.frame.size.width / 2.0)
        
        if !object.userName.isEmpty {
            cell.userTitle.text = object.userName.shiftFirstLetter()
        }
    }
}
