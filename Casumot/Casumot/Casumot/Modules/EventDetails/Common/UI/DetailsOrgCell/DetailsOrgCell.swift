//
//  DetailsOrgCell.swift
//  CasumoTest
//
//  Created by Rost on 8/1/19.
//  Copyright © 2019 Rost Gress. All rights reserved.
//

import Foundation
import UIKit


class DetailsOrgCell: UITableViewCell {
    @IBOutlet weak var avaImage: AsyncImageView!
    @IBOutlet weak var orgTitle: UILabel!
    @IBOutlet weak var urlLink: UILabel!
    var viewModel: DetailsOrgCellViewModel!
    
    /// ---> View life cycle <--- ///
    override func awakeFromNib() {
        super.awakeFromNib()
        
        viewModel = DetailsOrgCellViewModel()
    }
}
