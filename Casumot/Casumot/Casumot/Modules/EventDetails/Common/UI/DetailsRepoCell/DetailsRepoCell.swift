//
//  DetailsRepoCell.swift
//  CasumoTest
//
//  Created by Rost on 8/1/19.
//  Copyright © 2019 Rost Gress. All rights reserved.
//

import Foundation
import UIKit


class DetailsRepoCell: UITableViewCell {
    @IBOutlet weak var repoTitle: UILabel!
    @IBOutlet weak var repoUrl: UILabel!
    var viewModel: DetailsRepoCellViewModel!
    
    /// ---> View life cycle <--- ///
    override func awakeFromNib() {
        super.awakeFromNib()
        
        viewModel = DetailsRepoCellViewModel()
    }
}
