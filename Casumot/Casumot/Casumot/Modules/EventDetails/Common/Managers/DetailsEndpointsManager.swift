//
//  DetailsEndpointsManager.swift
//  Casumot
//
//  Created by Rost on 28.01.2020.
//  Copyright © 2020 Rost Gress. All rights reserved.
//

import Foundation
import UIKit


class DetailsEndpointsManager {
    
    /// ---> Load for info units <--- ///
    func loadInfo(_ view: DetailsViewController, url: String, type: UnitTypes) {
        UnitInfoLoader.loadInfo(url, type: type) { result in
            if let _ = result as? String {
                AlertPresenter.showAlert(view, message: "Download unit info error!")
            } else {
                switch type {
                    case .Actor:
                        if let object = result as? ActorInfoStruct {
                            DataContainer.shared.actorInfoUnit = object
                        }
                    
                    case .Org:
                        if let object = result as? OrgInfoStruct {
                            DataContainer.shared.orgInfoUnit = object
                        }
                }
            }
        }
    }
}
