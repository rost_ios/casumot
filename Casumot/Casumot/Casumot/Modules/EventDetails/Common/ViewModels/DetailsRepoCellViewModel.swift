//
//  DetailsRepoCellViewModel.swift
//  Casumot
//
//  Created by Rost on 28.01.2020.
//  Copyright © 2020 Rost Gress. All rights reserved.
//

import Foundation
import UIKit


class DetailsRepoCellViewModel {
    
    /// ---> Setter for values in UI <--- ///
    func setCellValues(_ cell: DetailsRepoCell, object: RepoStruct) {
        if !object.repoName.isEmpty {
            cell.repoTitle.text = "Repository: " + object.repoName
        }
        
        if !object.repoUrl.isEmpty {
            cell.repoUrl.text = "Url: " + object.repoUrl
        } else {
            cell.repoUrl.text = "Url: " + "---"
        }
    }
}
