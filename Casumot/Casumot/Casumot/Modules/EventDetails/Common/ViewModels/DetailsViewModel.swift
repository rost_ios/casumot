//
//  DetailsViewModel.swift
//  Casumot
//
//  Created by Rost on 28.01.2020.
//  Copyright © 2020 Rost Gress. All rights reserved.
//

import Foundation
import UIKit


class DetailsViewModel {
    
    /// ---> Add customizations for UI <--- ///
    func changeUI(_ view: DetailsViewController) {
        if let nc = view.navigationController {
            UIBuilder.makeBackItem(nc)
        }

        if let event = DataContainer.shared.selectedEvent {
            if !event.eventType.isEmpty {
                view.title = event.eventType
            }
            
            if let actor = event.eventActor, let manager = view.endpointsManager {
                if !actor.actorUrl.isEmpty {
                    manager.loadInfo(view, url: actor.actorUrl, type: .Actor)
                }
            }
            
            if let org = event.eventOrg, let manager = view.endpointsManager {
                if !org.orgUrl.isEmpty {
                    manager.loadInfo(view, url: org.orgUrl, type: .Org)
                }
            }
        }
        
        UIBuilder.addEmptyFooter(view.detailsTable)
    }
    
    
    /// ---> Add customizations for UI <--- ///
    func showUnitPage(_ view: DetailsViewController) {
        if let nc = view.navigationController {
            Router.showUnit(nc)
        }
    }
    
    
    /// ---> Function for make custom cells <--- ///
    func makeCell(_ table: UITableView, index: IndexPath) -> UITableViewCell {
        if let type = DetailsCells(rawValue: index.row) {
            switch type {
                case .Actor:
                    if let cell = table.dequeueReusableCell(withIdentifier: "DetailsActorCell",
                                                            for: index) as? DetailsActorCell {
                        if let event = DataContainer.shared.selectedEvent, let model = cell.viewModel {
                            if let actor = event.eventActor {
                                model.setCellValues(cell, object: actor)
                            }
                        }
                
                        return cell
                    }
                    
                    return  DetailsActorCell()
                case .Repo:
                    if let cell = table.dequeueReusableCell(withIdentifier: "DetailsRepoCell",
                                                            for: index) as? DetailsRepoCell {
                        if let event = DataContainer.shared.selectedEvent, let model = cell.viewModel {
                            if let repo = event.eventRepo {
                                model.setCellValues(cell, object: repo)
                            }
                        }
                        
                        return cell
                    }
                    
                    return DetailsRepoCell()
                case .Org:
                    if let cell = table.dequeueReusableCell(withIdentifier: "DetailsOrgCell",
                                                            for: index) as? DetailsOrgCell {
                        if let event = DataContainer.shared.selectedEvent, let model = cell.viewModel {
                            if let org = event.eventOrg {
                                model.setCellValues(cell, object: org)
                            }
                        }
                        
                        return cell
                    }
                    
                    return DetailsOrgCell()
            }
        }
        
        return UITableViewCell()
    }
    
    
    /// ---> Make height  of row <--- ///
    func makeRowHeight(_ index: IndexPath) -> CGFloat {
        if let type = DetailsCells(rawValue: index.row) {
            switch type {
                case .Actor:
                    return 140.0
                case .Repo:
                    return 85.0
                case .Org:
                    return 87.0
            }
        }
        
        return 0.0
    }
    
    
    /// ---> Make counts of events <--- ///
    func makeRowsCount() -> Int {
        return 3
    }
    
    
    /// ---> Function for present details view <--- ///
    func presentDetails(_ view: DetailsViewController, index: IndexPath) {
        if let type = DetailsCells(rawValue: index.row) {
            switch type {
                case .Actor:
                    if let actor = DataContainer.shared.actorInfoUnit {
                        if actor.htmlUrl.isEmpty && actor.followersUrl.isEmpty && actor.htmlUrl.isEmpty {
                            AlertPresenter.showAlert(view, message: "This actor have not a details for shoing.")
                        } else {
                            DataContainer.shared.selectedUnit = UnitTypes.Actor
                            showUnitPage(view)
                        }
                    }
                    
                case .Org:
                    if let org = DataContainer.shared.orgInfoUnit {
                        if org.homepageUrl.isEmpty && org.htmlUrl.isEmpty {
                            AlertPresenter.showAlert(view, message: "This organization have not a details for shoing.")
                        } else {
                            DataContainer.shared.selectedUnit = UnitTypes.Org
                            showUnitPage(view)
                        }
                    }
                    
                case .Repo:
                    break
            }
        }
    }
}
