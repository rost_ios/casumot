//
//  DetailsActorCellViewModel.swift
//  Casumot
//
//  Created by Rost on 28.01.2020.
//  Copyright © 2020 Rost Gress. All rights reserved.
//

import Foundation
import UIKit


class DetailsActorCellViewModel {
    
    /// ---> Setter for values in UI <--- ///
    func setCellValues(_ cell: DetailsActorCell, object: ActorStruct) {
        let holderImage = UIImage(named: "holder_image")
        
        if !object.avaUrl.isEmpty {
            cell.avaImage.loadAsync(object.avaUrl, placeholder: holderImage)
        } else {
           cell.avaImage.image = holderImage
        }
        
        cell.avaImage.roundCorners(cell.avaImage.frame.size.width / 2.0)
        
        if !object.actorLogin.isEmpty {
            cell.actorName.text = "Actor: " + object.actorLogin.shiftFirstLetter()
        }

        if !object.actorUrl.isEmpty {
            cell.urlLink.text = "Url: " + object.actorUrl
        } else {
            cell.urlLink.text = "Url: " + "---"
        }
    }
}
