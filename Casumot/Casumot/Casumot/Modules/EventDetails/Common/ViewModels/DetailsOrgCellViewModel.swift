//
//  DetailsOrgCellViewModel.swift
//  Casumot
//
//  Created by Rost on 28.01.2020.
//  Copyright © 2020 Rost Gress. All rights reserved.
//

import Foundation
import UIKit


class DetailsOrgCellViewModel {

    /// ---> Setter for values in UI <--- ///
    func setCellValues(_ cell: DetailsOrgCell, object: OrgStruct) {
        let holderImage = UIImage(named: "holder_image")
        
        if !object.avaUrl.isEmpty {
            cell.avaImage.loadAsync(object.avaUrl, placeholder: holderImage)
        } else {
            cell.avaImage.image = holderImage
        }
        
        cell.avaImage.roundCorners(cell.avaImage.frame.size.width / 2.0)
        
        if !object.orgLogin.isEmpty {
            cell.orgTitle.text = "Org. name: " + object.orgLogin.shiftFirstLetter()
        } else {
            cell.orgTitle.text = "Org. name: " + "---"
        }
        
        if !object.orgUrl.isEmpty {
            cell.urlLink.text = "Url: " + object.orgUrl
        } else {
            cell.urlLink.text = "Url: " + "---"
        }
    }
}
