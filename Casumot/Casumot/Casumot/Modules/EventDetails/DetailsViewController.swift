//
//  DetailsViewController.swift
//  CasumoTest
//
//  Created by Rost on 7/30/19.
//  Copyright © 2019 Rost Gress. All rights reserved.
//

import UIKit


class DetailsViewController: UIViewController {
    @IBOutlet weak var detailsTable: UITableView!
    var viewModel: DetailsViewModel!
    var endpointsManager: DetailsEndpointsManager!
    
    
    /// ---> View life cycle <--- ///
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        viewModel           = DetailsViewModel()
        endpointsManager    = DetailsEndpointsManager()
        
        viewModel.changeUI(self)
    }
}
