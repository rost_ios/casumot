//
//  WebViewModel.swift
//  Casumot
//
//  Created by Rost on 28.01.2020.
//  Copyright © 2020 Rost Gress. All rights reserved.
//

import Foundation
import UIKit


class WebViewModel {
    
    /// ---> Add customizations for UI <--- ///
    func setupUI(_ view: WebViewController) {
        if let nc = view.navigationController {
            UIBuilder.makeBackItem(nc)
        }
        
        if let value = DataContainer.shared.webTitle {
            view.title = value
        }
    }
    
    
    /// ---> Load web page via passed url <--- ///
    func loadWebPage(_ view: WebViewController, url: String) {
        if let link = URL(string: url) {
            view.webView.load(URLRequest(url: link))
        }
    }
}
