//
//  WebViewController.swift
//  CasumoTest
//
//  Created by Rost on 8/2/19.
//  Copyright © 2019 Rost Gress. All rights reserved.
//

import Foundation
import WebKit


class WebViewController: UIViewController {
    @IBOutlet weak var webView: WKWebView!
    var viewModel: WebViewModel!
    
    
    /// ---> View life cycle <--- ///
    override func viewDidLoad() {
        super.viewDidLoad()
        
        viewModel = WebViewModel()
        
        viewModel.setupUI(self)
    }
        
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        if let url = DataContainer.shared.webUrl {
            viewModel.loadWebPage(self, url: url)
        }
    }
    
    
    deinit {
        DataContainer.shared.webTitle   = nil   // Clean unused stored data
        DataContainer.shared.webUrl     = nil 
    }
}
