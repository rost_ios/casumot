//
//  EventsViewController.swift
//  CasumoTest
//
//  Created by Rost on 7/30/19.
//  Copyright © 2019 Rost Gress. All rights reserved.
//

import UIKit


class EventsViewController: UIViewController {
    @IBOutlet weak var eventsTable: UITableView!
    @IBOutlet weak var sortView: SortView!
    var refreshControl: UIRefreshControl!    
    var dataArray: [EventStruct]        = []
    var copyDataArray: [EventStruct]    = []
    var currentPage: Int                = 0
    var isLoadNextEvent: Bool           = true
    var viewModel: EventsViewModel!
    var endpointsManager: EventsEndpointsManager!
    var filterManager: EventsFilterManager!
    var observerManager: EventsObserverManager!
    
    
    /// ---> View life cycle <--- ///
    override func viewDidLoad() {
        super.viewDidLoad()
        
        viewModel           = EventsViewModel()
        endpointsManager    = EventsEndpointsManager()
        filterManager       = EventsFilterManager()
        observerManager     = EventsObserverManager()
                
        viewModel.setupUI(self)
        
        endpointsManager.loadEvents(self, page: 1)
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
               
        navigationController?.navigationBar.isHidden = true
        
        observerManager.addObservers(self)
    }
    
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        navigationController?.navigationBar.isHidden = false
        
        observerManager.removeObservers(self)
    }
    
    
    /// ---> Show or hide sort view <--- ///
    @IBAction func showHideSortView() {
        if let model = sortView.viewModel {
            model.showHideView(sortView)
        }
    }

    
    /// ---> Update new events from server <--- ///
    @objc func refreshEvents() {
        if let manager = endpointsManager {
            manager.loadEvents(self, page: 1)
        }
    }
    
    
    /// ---> Filter events by type <--- ///
    @objc func filterEvents() {
        filterManager.handleFilterEvents(self)
    }
}
