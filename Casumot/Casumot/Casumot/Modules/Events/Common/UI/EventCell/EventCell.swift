//
//  EventCell.swift
//  CasumoTest
//
//  Created by Rost on 7/30/19.
//  Copyright © 2019 Rost Gress. All rights reserved.
//

import Foundation
import UIKit


class EventCell: UITableViewCell {
    @IBOutlet weak var eventImage: AsyncImageView!
    @IBOutlet weak var eventType: UILabel!
    @IBOutlet weak var eventActor: UILabel!
    @IBOutlet weak var eventDate: UILabel!
    var viewModel: EventCellViewModel!
    
    /// ---> View life cycle <--- ///
    override func awakeFromNib() {
        super.awakeFromNib()
        
        viewModel = EventCellViewModel()
    }
}
