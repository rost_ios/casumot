//
//  EventsFilterManager.swift
//  Casumot
//
//  Created by Rost on 28.01.2020.
//  Copyright © 2020 Rost Gress. All rights reserved.
//

import Foundation
import UIKit


class EventsFilterManager {
    
    /// ---> Filter events by type <--- ///
    func handleFilterEvents(_ view: EventsViewController) {
        view.dataArray = view.copyDataArray       // Reset filtering if user cleaned selected filter
        
        if let selected = DataContainer.shared.selectedSortIndex {
            if let type = FilterTypes(rawValue: selected) {
                switch type {
                    case .DateAsc:
                        view.dataArray = view.copyDataArray.sorted(by: { $0.createdDate < $1.createdDate })
                    
                    case .DateDesc:
                        view.dataArray = view.copyDataArray.sorted(by: { $0.createdDate > $1.createdDate })
                    
                    case .TypeAsc:
                        view.dataArray = view.copyDataArray.sorted(by: { $0.eventType < $1.eventType })
                    
                    case .TypeDesc:
                        view.dataArray = view.copyDataArray.sorted(by: { $0.eventType > $1.eventType })
                    
                    case .ActorAsc:
                        view.dataArray = view.copyDataArray.sorted(by: {
                            guard let first: String  = $0.eventActor?.actorLogin.lowercased() else { return false }
                            guard let second: String = $1.eventActor?.actorLogin.lowercased() else { return true }
                            
                            return first < second
                        })
                    case .ActorDesc:
                        view.dataArray = view.copyDataArray.sorted(by: {
                            guard let first: String  = $0.eventActor?.actorLogin.lowercased() else { return false }
                            guard let second: String = $1.eventActor?.actorLogin.lowercased() else { return true }
                            
                            return first > second
                        })
                    
                    case .RepoAsc:
                        view.dataArray = view.copyDataArray.sorted(by: {
                            guard let first: String  = $0.eventRepo?.repoName.lowercased() else { return false }
                            guard let second: String = $1.eventRepo?.repoName.lowercased() else { return true }
                            
                            return first < second
                        })
                    
                    case .RepoDesc:
                        view.dataArray = view.copyDataArray.sorted(by: {
                            guard let first: String  = $0.eventRepo?.repoName.lowercased() else { return false }
                            guard let second: String = $1.eventRepo?.repoName.lowercased() else { return true }
                            
                            return first > second
                        })
                }
            }
        }
        
        view.eventsTable.reloadData()
    }
}
