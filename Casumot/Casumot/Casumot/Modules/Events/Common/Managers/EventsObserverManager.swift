//
//  EventsObserverManager.swift
//  Casumot
//
//  Created by Rost on 28.01.2020.
//  Copyright © 2020 Rost Gress. All rights reserved.
//

import Foundation
import UIKit


class EventsObserverManager {
    
    /// ---> Setter for observers <--- ///
    func addObservers(_ view: EventsViewController) {
        NotificationCenter.default.addObserver(view,
                                               selector: #selector(view.filterEvents),
                                               name: filterObserverName,
                                               object: nil)
    }
    
    
    /// ---> Destructor for observers <--- ///
    func removeObservers(_ view: EventsViewController) {
        NotificationCenter.default.removeObserver(view, name: filterObserverName, object: nil)
    }
}
