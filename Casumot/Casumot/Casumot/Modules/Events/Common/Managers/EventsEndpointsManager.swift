//
//  EventsEndpointsManager.swift
//  Casumot
//
//  Created by Rost on 28.01.2020.
//  Copyright © 2020 Rost Gress. All rights reserved.
//

import Foundation
import UIKit


class EventsEndpointsManager {
    
    /// ---> Load events function <--- ///
    func loadEvents(_ view: EventsViewController, page: Int) {
        EventsLoader.loadEvents(page) { result in
            if let events = result as? [EventStruct], let model = view.viewModel {
                model.refreshTable(view, events: events)
            } else {
                if let message = result as? String {
                    AlertPresenter.showAlert(view, message: message)
                } else {
                    AlertPresenter.showAlert(view, message: "Received error on download events!")
                }
                
                view.isLoadNextEvent = false
            }
        }
    }
}
