//
//  EventCellViewModel.swift
//  Casumot
//
//  Created by Rost on 28.01.2020.
//  Copyright © 2020 Rost Gress. All rights reserved.
//

import Foundation
import UIKit


class EventCellViewModel {
    
    /// ---> Setter for values in UI <--- ///
    func setCellValues(_ cell: EventCell, object: EventStruct) {
        let holderImage = UIImage(named: "holder_image")
        
        if let actor = object.eventActor {
            if !actor.avaUrl.isEmpty {
                cell.eventImage.loadAsync(actor.avaUrl, placeholder: holderImage)
            } else {
                cell.eventImage.image = holderImage
            }
            
            cell.eventImage.roundCorners(cell.eventImage.frame.size.width / 2.0)
            
            if !actor.actorLogin.isEmpty {
                let actorName: String = actor.actorLogin
                cell.eventActor.text = actorName.shiftFirstLetter()
            }
        }
        
        if !object.eventType.isEmpty {
            cell.eventType.text = object.eventType
        }
        
        if !object.createdDate.isEmpty {
            cell.eventDate.text = "Created: " + object.createdDate
        }
    }
}
