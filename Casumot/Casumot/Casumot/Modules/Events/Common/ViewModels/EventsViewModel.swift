//
//  EventsViewModel.swift
//  Casumot
//
//  Created by Rost on 28.01.2020.
//  Copyright © 2020 Rost Gress. All rights reserved.
//

import Foundation
import UIKit


class EventsViewModel {
        
    /// ---> Setup UI function <--- ///
    func setupUI(_ view: EventsViewController) {
        view.refreshControl = UIBuilder.makeRefreshControl(view, selector: "refreshEvents")
        if #available(iOS 10.0, *) {
            view.eventsTable.refreshControl = view.refreshControl
        } else {
            view.eventsTable.addSubview(view.refreshControl)
        }
    }
    
    
    /// ---> Refresh data source and reload table of events <--- ///
    func refreshTable(_ view: EventsViewController, events: [EventStruct]) {
        view.refreshControl.endRefreshing()
        
        view.dataArray       += events
        view.copyDataArray   += events
        
        view.eventsTable.reloadData()
    }
    
    
    /// ---> Make custom cell for table view <--- ///
    func makeCell(_ table: UITableView, array: [EventStruct], index: IndexPath) -> UITableViewCell {
        if let cell = table.dequeueReusableCell(withIdentifier: "EventCell", for: index) as? EventCell {
            let newsObject: EventStruct = array[index.row]
            
            if let model = cell.viewModel {
                model.setCellValues(cell, object: newsObject)
            }
            
            return cell
        }
        
        return EventCell()
    }
    
    
    /// ---> Make counts of events <--- ///
    func makeRowsCount(_ view: EventsViewController) -> Int {
        return view.dataArray.count
    }
    
    
    /// ---> Make height  of row <--- ///
    func makeRowHeight() -> CGFloat {
        return 74.0
    }
    
    
    /// ---> Function for present details view <--- ///
    func presentDetails(_ view: EventsViewController, index: IndexPath) {
        let selectedObject = view.dataArray[index.row]
        
        DataContainer.shared.selectedEvent = selectedObject
        
        if let nc = view.navigationController {
            Router.showDetails(nc)
        }
    }
    
    
    /// ---> Function for handle actions on scroll view <--- ///
    func handleScroll(_ view: EventsViewController, scroll: UIScrollView) {
        let height = scroll.frame.size.height
        let contentYoffset = scroll.contentOffset.y
        let distanceFromBottom = scroll.contentSize.height - contentYoffset
        if distanceFromBottom < height {                            // Check bottom of table
            let nextPage = (view.dataArray.count / 30) + 1
            if view.currentPage < nextPage {                             // Sentinel check for downloading
                view.endpointsManager.loadEvents(view, page: nextPage)   // Download next events
                
                view.currentPage = nextPage                              // Set next page
            } else {
                if view.currentPage == nextPage && view.isLoadNextEvent {     // Sentinel check for downloading
                    view.currentPage += 1
                    
                    view.endpointsManager.loadEvents(view, page: view.currentPage)   // Download next events
                }
            }
        }
    }
}
