//
//  EventsViewController+ScrollView.swift
//  CasumoTest
//
//  Created by Rost on 8/1/19.
//  Copyright © 2019 Rost Gress. All rights reserved.
//

import Foundation
import UIKit


extension EventsViewController {
        
    /// ---> ScrollView delegate function <--- ///
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        viewModel.handleScroll(self, scroll: scrollView)
    }
}
