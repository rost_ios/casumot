//
//  UnitEndpointsManager.swift
//  Casumot
//
//  Created by Rost on 28.01.2020.
//  Copyright © 2020 Rost Gress. All rights reserved.
//

import Foundation
import UIKit


class UnitEndpointsManager {
    
    /// ---> Load for info units <--- ///
    func loadFollowers(_ view: UnitViewController, url: String) {
        FollowersLoader.loadFollowers(url) { result in
            if let followers = result as? [FollowerUserStruct] {
                if followers.count > 0 {
                    DataContainer.shared.followersArray = followers
                    if let model = view.viewModel {
                        model.showFollowersPage(view)
                    }
                } else {
                    AlertPresenter.showAlert(view, message: "List of followers is empty!")
                }
            } else {
                if let message = result as? String {
                    AlertPresenter.showAlert(view, message: message)
                } else {
                    AlertPresenter.showAlert(view, message: "Download followers unknown error!")
                }
            }
        }
    }
}
