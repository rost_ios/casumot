//
//  UnitCell.swift
//  CasumoTest
//
//  Created by Rost on 8/2/19.
//  Copyright © 2019 Rost Gress. All rights reserved.
//

import Foundation
import UIKit


class UnitCell: UITableViewCell {
    @IBOutlet weak var unitTitle: UILabel!
    var viewModel: UnitCellViewModel!
    
    /// ---> View life cycle <--- ///
    override func awakeFromNib() {
        super.awakeFromNib()
        
        viewModel = UnitCellViewModel()
    }
}
