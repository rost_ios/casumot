//
//  UnitCellViewModel.swift
//  Casumot
//
//  Created by Rost on 28.01.2020.
//  Copyright © 2020 Rost Gress. All rights reserved.
//

import Foundation
import UIKit


class UnitCellViewModel {
    
    /// ---> Setter for values in UI <--- ///
    func setCellValues(_ cell: UnitCell, title: String) {
        if title == "Html" {
            cell.unitTitle.text = "Web page"
        } else {
            cell.unitTitle.text = title
        }
    }
}
