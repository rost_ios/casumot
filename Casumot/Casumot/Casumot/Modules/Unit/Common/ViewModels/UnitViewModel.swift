//
//  UnitViewModel.swift
//  Casumot
//
//  Created by Rost on 28.01.2020.
//  Copyright © 2020 Rost Gress. All rights reserved.
//

import Foundation
import UIKit


class UnitViewModel {
    
    /// ---> Add customizations for UI <--- ///
    func changeUI(_ view: UnitViewController) {
        if let nc = view.navigationController {
            UIBuilder.makeBackItem(nc)
        }
        
        if let unit = DataContainer.shared.selectedUnit {
            switch unit {
                case .Actor:
                    if let event = DataContainer.shared.selectedEvent {
                        if let actor = event.eventActor {
                            let actorName = actor.actorLogin
                            
                            if !actorName.isEmpty {
                                view.title = "Actor: " + actorName.shiftFirstLetter()
                            } else {
                                view.title = "Actor"
                            }
                        } else {
                            view.title = "Actor"
                        }
                    }
                
                case .Org:
                    if let event = DataContainer.shared.selectedEvent {
                        if let org = event.eventOrg {
                            if !org.orgLogin.isEmpty {
                                view.title = "Organization: " + org.orgLogin.shiftFirstLetter()
                            } else {
                                view.title = "Organization"
                            }
                        } else {
                            view.title = "Organization"
                        }
                    }
            }
        }
        
        UIBuilder.addEmptyFooter(view.unitTable)
    }
    
    
    /// ---> Constructor for dataSource <--- ///
    func makeDataSource(_ view: UnitViewController) {
        if let unit = DataContainer.shared.selectedUnit {
            var mirror: Mirror!
            
            switch unit {
                case .Actor:
                    if let actor = DataContainer.shared.actorInfoUnit {
                        mirror = Mirror(reflecting: actor)
                    }
                case .Org:
                    if let org = DataContainer.shared.orgInfoUnit {
                        mirror = Mirror(reflecting: org)
                    }
            }
            
            for child in mirror.children  {
                if let key = child.label {
                    if let value = child.value as? String {
                        if !value.isEmpty {
                            let titleString = key.replacingOccurrences(of: "Url",
                                                                       with: "")
                            let resultTitle = titleString.shiftFirstLetter()

                            view.dataArray.append(resultTitle)
                            view.linksArray.append(value)
                        }
                    }
                }
            }
            
            view.unitTable.reloadData()
        }
    }
    
    
    /// ---> Show followers page <--- ///
    func showFollowersPage(_ view: UnitViewController) {
        if let nc = view.navigationController {
            Router.showFollowers(nc)
        }
    }
    
    
    /// ---> Function for make custom cell <--- ///
    func makeCell(_ table: UITableView, array: [String], index: IndexPath) -> UITableViewCell {
        if let cell = table.dequeueReusableCell(withIdentifier: "UnitCell", for: index) as? UnitCell {
            let title = array[index.row]
            
            if let model = cell.viewModel {
                model.setCellValues(cell, title: title)
            }
            
            return cell
        }
        
        return UnitCell()
    }
    
    
    /// ---> Make height  of row <--- ///
    func makeRowHeight() -> CGFloat {
        return 60.0
    }
    
    
    /// ---> Make counts of events <--- ///
    func makeRowsCount(_ view: UnitViewController) -> Int {
        return view.dataArray.count
    }
    
    
    /// ---> Fucntions for present details view <--- ///
    func presentDetails(_ view: UnitViewController, index: IndexPath) {
        let selectedTitle = view.dataArray[index.row]
        
        switch selectedTitle {
            case "Blog", "Html":
                var webTitle = view.dataArray[index.row]
                if webTitle == "Html" {
                    webTitle = "Web page"
                }
            
                DataContainer.shared.webTitle = webTitle
                DataContainer.shared.webUrl   = view.linksArray[index.row]
            
                if let nc = view.navigationController {
                    Router.showWeb(nc)
                }
            
            case "Followers":
                if let manager = view.endpointsManager {
                    manager.loadFollowers(view, url: view.linksArray[index.row])
                }
        
            default:
                break
        }
    }
}
