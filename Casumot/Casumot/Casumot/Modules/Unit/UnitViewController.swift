//
//  UnitViewController.swift
//  CasumoTest
//
//  Created by Rost on 8/2/19.
//  Copyright © 2019 Rost Gress. All rights reserved.
//

import Foundation
import UIKit


class UnitViewController: UIViewController {
    @IBOutlet weak var unitTable: UITableView!
    var dataArray: [String]  = []
    var linksArray: [String] = []
    var viewModel: UnitViewModel!
    var endpointsManager: UnitEndpointsManager!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        viewModel           =  UnitViewModel()
        endpointsManager    = UnitEndpointsManager()
        
        viewModel.changeUI(self)
        
        viewModel.makeDataSource(self)
    }
}
