//
//  Data+Convert.swift
//  CasumoTest
//
//  Created by Rost on 7/30/19.
//  Copyright © 2019 Rost Gress. All rights reserved.
//

import Foundation


extension Date {
    
    
    /// ---> Function for convert data to other format  <--- ///
    static func convert(_ string: String, income: String, result: String) -> String {
        let dateFormater = DateFormatter()
        dateFormater.timeZone = NSTimeZone(abbreviation: "UTC") as TimeZone?
        dateFormater.dateFormat = income
        let date = dateFormater.date(from: string)
        
        dateFormater.timeZone = NSTimeZone.system
        
        dateFormater.dateFormat = result
        
        let timeResult = dateFormater.string(from: date!)
        
        return timeResult
    }
}
