//
//  String+Uppercased.swift
//  CasumoTest
//
//  Created by Rost on 7/31/19.
//  Copyright © 2019 Rost Gress. All rights reserved.
//

import Foundation


extension String {
    
    
    /// ---> Functions for uppercase first letter in string  <--- ///
    func shiftFirstLetter() -> String {
        return prefix(1).uppercased() + self.lowercased().dropFirst()
    }

    mutating func shiftFirstLetter() {
        self = self.shiftFirstLetter()
    }
}
