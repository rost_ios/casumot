//
//  EventsLoader.swift
//  CasumoTest
//
//  Created by Rost on 7/30/19.
//  Copyright © 2019 Rost Gress. All rights reserved.
//

import Foundation


class EventsLoader {
    
    
    /// ---> Function for load events from API <--- ///
    static func loadEvents(_ page: Int, completion: ((_ response: AnyObject) -> Void)?) {
        let authParams  = "?\(clientParam)=\(Constants.getClientId())&\(secretParam)=\(Constants.getSecret())"
        let pagesParams = "&\(pageParam)=\(page)&\(perPageParam)=30"
        
        let urlString   = "\(baseUrl)/\(eventsParam)" + authParams + pagesParams

        guard let resultUrl = URL(string: urlString) else {
            print("Error: cannot create URL")
            return
        }
        
        let urlRequest = URLRequest(url: resultUrl)
        
        let config = URLSessionConfiguration.default
        let session = URLSession(configuration: config)
        
        let task = session.dataTask(with: urlRequest, completionHandler: { (data, response, error) in
            guard let data = data, error == nil else {
                print(error ?? "Unknown error")
                completion!(false as AnyObject)
                return
            }
            
            DispatchQueue.global(qos: .userInitiated).async {
                do {
                    if let array = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [[String: Any]] {
                    
                        let handledEvents = JsonHandler.handleEvents(array)
                        
                        DispatchQueue.main.async {
                            completion!(handledEvents as AnyObject)
                        }
                    } else {
                        if let values = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String: Any] {
                            
                            if let message = values["message"] {
                                DispatchQueue.main.async {
                                    completion!(message as AnyObject)
                                }
                            } else {
                                DispatchQueue.main.async {
                                    completion!("You received a limit of events." as AnyObject)
                                }
                            }
                        }
                    }
                } catch {
                    print(error)
                    DispatchQueue.main.async {
                        completion!(false as AnyObject)
                    }
                }
            }
        })
        
        task.resume()
    }
}
