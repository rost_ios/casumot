//
//  UnitInfoLoader.swift
//  CasumoTest
//
//  Created by Rost on 8/2/19.
//  Copyright © 2019 Rost Gress. All rights reserved.
//

import Foundation


class UnitInfoLoader {
    
    
    /// ---> Function for load unit info from API <--- ///
    static func loadInfo(_ url: String, type: UnitTypes, completion: ((_ response: AnyObject) -> Void)?) {
        
        guard let resultUrl = URL(string: url) else {
            print("Error: cannot create URL")
            return
        }
        
        let urlRequest = URLRequest(url: resultUrl)
        
        let config = URLSessionConfiguration.default
        let session = URLSession(configuration: config)
        
        let task = session.dataTask(with: urlRequest, completionHandler: { (data, response, error) in
            guard let data = data, error == nil else {
                print(error ?? "Unknown error")
                completion!(false as AnyObject)
                return
            }
            
            DispatchQueue.global(qos: .userInitiated).async {
                do {
                    if let values = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String: Any] {
                        
                        var handledObject: AnyObject!
                        
                        switch type {
                            case .Actor:
                                handledObject = JsonHandler.handleActor(values) as AnyObject
                            case .Org:
                                handledObject = JsonHandler.handleOrg(values) as AnyObject
                        }
                        
                        DispatchQueue.main.async {
                            completion!(handledObject as AnyObject)
                        }
                    } else {
                        DispatchQueue.main.async {
                            completion!("Received unknown error." as AnyObject)
                        }
                    }
                } catch {
                    print(error)
                    DispatchQueue.main.async {
                        completion!(false as AnyObject)
                    }
                }
            }
        })
        
        task.resume()
    }
}
