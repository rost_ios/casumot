//
//  FollowersLoader.swift
//  CasumoTest
//
//  Created by Rost on 8/2/19.
//  Copyright © 2019 Rost Gress. All rights reserved.
//

import Foundation


class FollowersLoader {
    
    
    /// ---> Function for load followers from API <--- ///
    static func loadFollowers(_ url: String, completion: ((_ response: AnyObject) -> Void)?) {
        
        guard let resultUrl = URL(string: url) else {
            print("Error: cannot create URL")
            return
        }
        
        let urlRequest = URLRequest(url: resultUrl)
        
        let config = URLSessionConfiguration.default
        let session = URLSession(configuration: config)
        
        let task = session.dataTask(with: urlRequest, completionHandler: { (data, response, error) in
            guard let data = data, error == nil else {
                print(error ?? "Unknown error")
                completion!(false as AnyObject)
                return
            }
            
            DispatchQueue.global(qos: .userInitiated).async {
                do {
                    if let array = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [[String: Any]] {
                        
                        let handledEvents = JsonHandler.handleFollowers(array)
                        
                        DispatchQueue.main.async {
                            completion!(handledEvents as AnyObject)
                        }
                    } else {
                        DispatchQueue.main.async {
                            completion!("You received system API error." as AnyObject)
                        }
                    }
                } catch {
                    print(error)
                    DispatchQueue.main.async {
                        completion!(false as AnyObject)
                    }
                }
            }
        })
        
        task.resume()
    }
}
