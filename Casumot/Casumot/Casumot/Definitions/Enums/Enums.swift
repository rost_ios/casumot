//
//  Enums.swift
//  CasumoTest
//
//  Created by Rost on 8/1/19.
//  Copyright © 2019 Rost Gress. All rights reserved.
//

import Foundation


enum FilterTypes: Int {
    case DateAsc = 0
    case DateDesc
    case TypeAsc
    case TypeDesc
    case ActorAsc
    case ActorDesc
    case RepoAsc
    case RepoDesc
}


enum DetailsCells: Int {
    case Actor = 0
    case Repo
    case Org
}


enum UnitTypes: Int {
    case Actor = 0
    case Org
}

