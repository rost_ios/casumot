//
//  Constants.swift
//  CasumoTest
//
//  Created by Rost on 7/30/19.
//  Copyright © 2019 Rost Gress. All rights reserved.
//

import Foundation
import UIKit


/// API urls and params
let baseUrl         = "https://api.github.com"
let eventsParam     = "events"
let pageParam       = "page"
let perPageParam    = "per_page"
let clientParam     = "client_id"
let secretParam     = "client_secret"


/// Custom colors
let refreshControlColor = UIColor(red: 0.71, green: 0.71, blue: 0.88, alpha: 1.0)


/// Observers names
let filterObserverName  = Notification.Name("NeedApplyFiltering")


class Constants {
    
    
    /// ---> Сlient id getter for authorization requests  <--- ///
    static func getClientId() -> String {
        return "91a95ef3e4d34a7ce3b5"
    }
    
    
    /// ---> Сlient secret getter for authorization requests  <--- ///
    static func getSecret() -> String {
        return "22f6ed400deb52ace7072b813345cb5e8524f70a"
    }
}
